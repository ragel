/*
 *  Copyright 2001-2007 Adrian Thurston <thurston@cs.queensu.ca>
 */

/*  This file is part of Ragel.
 *
 *  Ragel is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  Ragel is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with Ragel; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sstream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#ifdef _WIN32
#include <windows.h>
#include <psapi.h>
#include <time.h>
#include <io.h>
#include <process.h>

#if _MSC_VER
#define S_IRUSR _S_IREAD
#define S_IWUSR _S_IWRITE
#endif
#endif

/* Parsing. */
#include "ragel.h"
#include "rlscan.h"

/* Parameters and output. */
#include "pcheck.h"
#include "vector.h"
#include "version.h"
#include "common.h"
#include "xmlparse.h"

using std::istream;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::ios;
using std::streamsize;

/* Controls minimization. */
MinimizeLevel minimizeLevel = MinimizePartition2;
MinimizeOpt minimizeOpt = MinimizeMostOps;

/* Graphviz dot file generation. */
const char *machineSpec = 0, *machineName = 0;
bool machineSpecFound = false;
bool wantDupsRemoved = true;

bool printStatistics = false;
bool frontendOnly = false;
bool generateDot = false;

/* Target language and output style. */
CodeStyleEnum codeStyle = GenTables;

int numSplitPartitions = 0;
bool noLineDirectives = false;

bool displayPrintables = false;
bool graphvizDone = false;

/* Target ruby impl */
RubyImplEnum rubyImpl = MRI;

ArgsVector includePaths;

istream *inStream = 0;
ostream *outStream = 0;
output_filter *outFilter = 0;
const char *outputFileName = 0;

/* Print a summary of the options. */
void usage()
{
	cout <<
"usage: ragel [options] file\n"
"general:\n"
"   -h, -H, -?, --help   Print this usage and exit\n"
"   -v, --version        Print version information and exit\n"
"   -o <file>            Write output to <file>\n"
"   -s                   Print some statistics on stderr\n"
"   -d                   Do not remove duplicates from action lists\n"
"   -I <dir>             Add <dir> to the list of directories to search\n"
"                        for included an imported files\n"
"error reporting format:\n"
"   --error-format=gnu   file:line:column: message (default)\n"
"   --error-format=msvc  file(line,column): message\n"
"fsm minimization:\n"
"   -n                   Do not perform minimization\n"
"   -m                   Minimize at the end of the compilation\n"
"   -l                   Minimize after most operations (default)\n"
"   -e                   Minimize after every operation\n"
"visualization:\n"
"   -x                   Run the frontend only: emit XML intermediate format\n"
"   -V                   Generate a dot file for Graphviz\n"
"   -p                   Display printable characters on labels\n"
"   -S <spec>            FSM specification to output (for rlgen-dot)\n"
"   -M <machine>         Machine definition/instantiation to output (for rlgen-dot)\n"
"host language:\n"
"   -C                   The host language is C, C++, Obj-C or Obj-C++ (default)\n"
"   -D                   The host language is D\n"
"   -J                   The host language is Java\n"
"   -R                   The host language is Ruby\n"
"   -A                   The host language is C#\n"
"line direcives: (C/D/C# only)\n"
"   -L                   Inhibit writing of #line directives\n"
"code style: (C/Ruby/C# only)\n"
"   -T0                  Table driven FSM (default)\n"
"   -T1                  Faster table driven FSM\n"
"   -F0                  Flat table driven FSM\n"
"   -F1                  Faster flat table-driven FSM\n"
"code style: (C/C# only)\n"
"   -G0                  Goto-driven FSM\n"
"   -G1                  Faster goto-driven FSM\n"
"code style: (C only)\n"
"   -G2                  Really fast goto-driven FSM\n"
"   -P<N>                N-Way Split really fast goto-driven FSM\n"
	;	

	exit(0);
}

/* Print version information and exit. */
void version()
{
	cout << "Ragel State Machine Compiler version " VERSION << " " PUBDATE << endl <<
			"Copyright (c) 2001-2007 by Adrian Thurston" << endl;
	exit(0);
}

/* Error reporting format. */
ErrorFormat errorFormat = ErrorFormatGNU;

InputLoc makeInputLoc( const char *fileName, int line, int col)
{
	InputLoc loc = { fileName, line, col };
	return loc;
}

ostream &operator<<( ostream &out, const InputLoc &loc )
{
	assert( loc.fileName != 0 );
	switch ( errorFormat ) {
	case ErrorFormatMSVC:
		out << loc.fileName << "(" << loc.line;
		if ( loc.col )
			out << "," << loc.col;
		out << ")";
		break;

	default:
		out << loc.fileName << ":" << loc.line;
		if ( loc.col )
			out << ":" << loc.col;
		break;
	}
	return out;
}

/* Total error count. */
int gblErrorCount = 0;

/* Print the opening to a warning in the input, then return the error ostream. */
ostream &warning( const InputLoc &loc )
{
	cerr << loc << ": warning: ";
	return cerr;
}

/* Print the opening to a program error, then return the error stream. */
ostream &error()
{
	gblErrorCount += 1;
	cerr << PROGNAME ": ";
	return cerr;
}

ostream &error( const InputLoc &loc )
{
	gblErrorCount += 1;
	cerr << loc << ": ";
	return cerr;
}

void escapeLineDirectivePath( std::ostream &out, char *path )
{
	for ( char *pc = path; *pc != 0; pc++ ) {
		if ( *pc == '\\' )
			out << "\\\\";
		else
			out << *pc;
	}
}

void processArgs( int argc, const char **argv, const char *&inputFileName )
{
	ParamCheck pc("xo:dnmleabjkS:M:I:CDJRAvHh?-:sT:F:G:P:LpV", argc, argv);

	/* FIXME: Need to check code styles VS langauge. */

	while ( pc.check() ) {
		switch ( pc.state ) {
		case ParamCheck::match:
			switch ( pc.parameter ) {
			case 'V':
				generateDot = true;
				break;

			case 'x':
				frontendOnly = true;
				break;

			/* Output. */
			case 'o':
				if ( *pc.paramArg == 0 )
					error() << "a zero length output file name was given" << endl;
				else if ( outputFileName != 0 )
					error() << "more than one output file name was given" << endl;
				else {
					/* Ok, remember the output file name. */
					outputFileName = pc.paramArg;
				}
				break;

			/* Flag for turning off duplicate action removal. */
			case 'd':
				wantDupsRemoved = false;
				break;

			/* Minimization, mostly hidden options. */
			case 'n':
				minimizeOpt = MinimizeNone;
				break;
			case 'm':
				minimizeOpt = MinimizeEnd;
				break;
			case 'l':
				minimizeOpt = MinimizeMostOps;
				break;
			case 'e':
				minimizeOpt = MinimizeEveryOp;
				break;
			case 'a':
				minimizeLevel = MinimizeApprox;
				break;
			case 'b':
				minimizeLevel = MinimizeStable;
				break;
			case 'j':
				minimizeLevel = MinimizePartition1;
				break;
			case 'k':
				minimizeLevel = MinimizePartition2;
				break;

			/* Machine spec. */
			case 'S':
				if ( *pc.paramArg == 0 )
					error() << "please specify an argument to -S" << endl;
				else if ( machineSpec != 0 )
					error() << "more than one -S argument was given" << endl;
				else {
					/* Ok, remember the path to the machine to generate. */
					machineSpec = pc.paramArg;
				}
				break;

			/* Machine path. */
			case 'M':
				if ( *pc.paramArg == 0 )
					error() << "please specify an argument to -M" << endl;
				else if ( machineName != 0 )
					error() << "more than one -M argument was given" << endl;
				else {
					/* Ok, remember the machine name to generate. */
					machineName = pc.paramArg;
				}
				break;

			case 'I':
				if ( *pc.paramArg == 0 )
					error() << "please specify an argument to -I" << endl;
				else {
					includePaths.append( pc.paramArg );
				}
				break;

			/* Host language types. */
			case 'C':
				hostLang = &hostLangC;
				break;
			case 'D':
				hostLang = &hostLangD;
				break;
			case 'J':
				hostLang = &hostLangJava;
				break;
			case 'R':
				hostLang = &hostLangRuby;
				break;
			case 'A':
				hostLang = &hostLangCSharp;
				break;

			/* Version and help. */
			case 'v':
				version();
				break;
			case 'H': case 'h': case '?':
				usage();
				break;
			case 's':
				printStatistics = true;
				break;
			case '-': {
				char *eq = strchr( pc.paramArg, '=' );

				if ( eq != 0 )
					*eq++ = 0;

				if ( strcmp( pc.paramArg, "help" ) == 0 )
					usage();
				else if ( strcmp( pc.paramArg, "version" ) == 0 )
					version();
				else if ( strcmp( pc.paramArg, "error-format" ) == 0 ) {
					if ( eq == 0 )
						error() << "expecting '=value' for error-format" << endl;
					else if ( strcmp( eq, "gnu" ) == 0 )
						errorFormat = ErrorFormatGNU;
					else if ( strcmp( eq, "msvc" ) == 0 )
						errorFormat = ErrorFormatMSVC;
					else
						error() << "invalid value for error-format" << endl;
				}
				else if ( strcmp( pc.paramArg, "rbx" ) == 0 )
					rubyImpl = Rubinius;
				else {
					error() << "--" << pc.paramArg << 
							" is an invalid argument" << endl;
				}
				break;
			}

			/* Passthrough args. */
			case 'T': 
				if ( pc.paramArg[0] == '0' )
					codeStyle = GenTables;
				else if ( pc.paramArg[0] == '1' )
					codeStyle = GenFTables;
				else {
					error() << "-T" << pc.paramArg[0] << 
							" is an invalid argument" << endl;
					exit(1);
				}
				break;
			case 'F': 
				if ( pc.paramArg[0] == '0' )
					codeStyle = GenFlat;
				else if ( pc.paramArg[0] == '1' )
					codeStyle = GenFFlat;
				else {
					error() << "-F" << pc.paramArg[0] << 
							" is an invalid argument" << endl;
					exit(1);
				}
				break;
			case 'G': 
				if ( pc.paramArg[0] == '0' )
					codeStyle = GenGoto;
				else if ( pc.paramArg[0] == '1' )
					codeStyle = GenFGoto;
				else if ( pc.paramArg[0] == '2' )
					codeStyle = GenIpGoto;
				else {
					error() << "-G" << pc.paramArg[0] << 
							" is an invalid argument" << endl;
					exit(1);
				}
				break;
			case 'P':
				codeStyle = GenSplit;
				numSplitPartitions = atoi( pc.paramArg );
				break;

			case 'p':
				displayPrintables = true;
				break;

			case 'L':
				noLineDirectives = true;
				break;
			}
			break;

		case ParamCheck::invalid:
			error() << "-" << pc.parameter << " is an invalid argument" << endl;
			break;

		case ParamCheck::noparam:
			/* It is interpreted as an input file. */
			if ( *pc.curArg == 0 )
				error() << "a zero length input file name was given" << endl;
			else if ( inputFileName != 0 )
				error() << "more than one input file name was given" << endl;
			else {
				/* OK, Remember the filename. */
				inputFileName = pc.curArg;
			}
			break;
		}
	}
}

void process( const char *inputFileName, const char *intermed )
{
	const char *xmlFileName = intermed;
	bool wantComplete = true;
	bool outputActive = true;

	/* Open the input file for reading. */
	assert( inputFileName != 0 );
	ifstream *inFile = new ifstream( inputFileName );
	if ( ! inFile->is_open() )
		error() << "could not open " << inputFileName << " for reading" << endp;

	/* Used for just a few things. */
	std::ostringstream hostData;

	if ( machineSpec == 0 && machineName == 0 )
		hostData << "<host line=\"1\" col=\"1\">";

	Scanner scanner( inputFileName, *inFile, hostData, 0, 0, 0, false );
	scanner.do_scan();

	/* Finished, final check for errors.. */
	if ( gblErrorCount > 0 )
		exit(1);
	
	/* Now send EOF to all parsers. */
	terminateAllParsers();

	/* Finished, final check for errors.. */
	if ( gblErrorCount > 0 )
		exit(1);

	if ( machineSpec == 0 && machineName == 0 )
		hostData << "</host>\n";

	if ( gblErrorCount > 0 )
		exit(1);
	
	/* Open the XML file for writing. */
	ostream *xmlOutFile = new ofstream( xmlFileName );

	/* Open the XML file for reading. */
	ifstream *xmlInFile = new ifstream( xmlFileName );
	if ( ! xmlInFile->is_open() )
		error() << "could not open " << xmlFileName << " for reading" << endl;

	/* Bail on above error. */
	if ( gblErrorCount > 0 )
		exit(1);

	/* Locate the backend program */
	if ( generateDot ) {
		wantComplete = false;
		outputActive = false;
	}

	XmlScanner xmlScanner( xmlFileName, *xmlInFile );
	XmlParser xmlParser( xmlFileName, outputActive, wantComplete );
	xmlParser.init();

	/* Write the machines, then the surrounding code. */
	writeMachines( *xmlOutFile, hostData.str(), inputFileName, xmlParser );

	/* Close the input and the intermediate file. */
	delete xmlOutFile;
	delete inFile;

	/* Bail on above error. */
	if ( gblErrorCount > 0 )
		exit(1);

	xml_parse( *xmlInFile, xmlFileName, 
		outputActive, wantComplete,
		xmlScanner, xmlParser );

	/* If writing to a file, delete the ostream, causing it to flush.
	 * Standard out is flushed automatically. */
	if ( outputFileName != 0 ) {
		delete outStream;
		delete outFilter;
	}

	/* Finished, final check for errors.. */
	if ( gblErrorCount > 0 ) {
		/* If we opened an output file, remove it. */
		if ( outputFileName != 0 )
			unlink( outputFileName );
		exit(1);
	}
}

char *makeIntermedTemplate( const char *baseFileName )
{
	char *result = 0;
	const char *templ = "ragel-XXXXXX.xml";
	char *lastSlash = strrchr( baseFileName, '/' );
	if ( lastSlash == 0 ) {
		result = new char[strlen(templ)+1];
		strcpy( result, templ );
	}
	else {
		int baseLen = lastSlash - baseFileName + 1;
		result = new char[baseLen + strlen(templ) + 1];
		memcpy( result, baseFileName, baseLen );
		strcpy( result+baseLen, templ );
	}
	return result;
};

const char *openIntermed( const char *inputFileName, const char *outputFileName )
{
	srand(time(0));
	const char *result = 0;

	/* Which filename do we use as the base? */
	const char *baseFileName = outputFileName != 0 ? outputFileName : inputFileName;

	/* The template for the intermediate file name. */
	const char *intermedFileName = makeIntermedTemplate( baseFileName );

	/* Randomize the name and try to open. */
	char fnChars[] = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char *firstX = strrchr( intermedFileName, 'X' ) - 5;
	for ( int tries = 0; tries < 20; tries++ ) {
		/* Choose a random name. */
		for ( int x = 0; x < 6; x++ )
			firstX[x] = fnChars[rand() % 52];

		/* Try to open the file. */
		int fd = ::open( intermedFileName, O_WRONLY|O_EXCL|O_CREAT, S_IRUSR|S_IWUSR );

		if ( fd > 0 ) {
			/* Success. Close the file immediately and return the name for use
			 * by the child processes. */
			::close( fd );
			result = intermedFileName;
			break;
		}

		if ( errno == EACCES ) {
			error() << "failed to open temp file " << intermedFileName << 
					", access denied" << endp;
		}
	}

	if ( result == 0 )
		error() << "abnormal error: cannot find unique name for temp file" << endp;

	return result;
}


void cleanExit( const char *intermed, int status )
{
	unlink( intermed );
	exit( status );
}

/* Main, process args and call yyparse to start scanning input. */
int main( int argc, const char **argv )
{
	const char *inputFileName = 0;
	processArgs( argc, argv, inputFileName );

	/* If -M or -S are given and we're not generating a dot file then invoke
	 * the frontend. These options are not useful with code generators. */
	if ( machineName != 0 || machineSpec != 0 ) {
		if ( !generateDot )
			frontendOnly = true;
	}

	/* Require an input file. If we use standard in then we won't have a file
	 * name on which to base the output. */
	if ( inputFileName == 0 )
		error() << "no input file given" << endl;

	/* Bail on argument processing errors. */
	if ( gblErrorCount > 0 )
		exit(1);

	/* Make sure we are not writing to the same file as the input file. */
	if ( inputFileName != 0 && outputFileName != 0 && 
			strcmp( inputFileName, outputFileName  ) == 0 )
	{
		error() << "output file \"" << outputFileName  << 
				"\" is the same as the input file" << endp;
	}

	const char *intermed = openIntermed( inputFileName, outputFileName );
	process( inputFileName, intermed );

	/* Clean up the intermediate. */
	cleanExit( intermed, 0 );

	return 0;
}
